package controlador;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import modelo.Assets;
import modelo.CargarImagenes;
import modelo.Personaje;
import modelo.Posicion;

public class Teclado implements KeyListener {
	
	private boolean[] keys = new boolean[256];	
	public static boolean UP, LEFT, DOWN, RIGHT;
    private boolean enMovimiento = false;
	
	public Teclado() {
		UP = false;
		LEFT = false;
		DOWN = false;
		RIGHT = false;
	}
	
	public void update() {
		UP = keys[KeyEvent.VK_W];
		LEFT = keys[KeyEvent.VK_A];
		DOWN = keys[KeyEvent.VK_S];
		RIGHT = keys[KeyEvent.VK_D];		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		keys[e.getKeyCode()] = true;
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys[e.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	public boolean isEnMovimiento() {
		return enMovimiento;
	}

	public void setEnMovimiento(boolean enMovimiento) {
		this.enMovimiento = enMovimiento;
	}
	
	
	
	

}