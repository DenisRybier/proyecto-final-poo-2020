package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import vista.VistaJuego;
import vista.VistaMenu;


public class ControladorMenu implements ActionListener {
 VistaMenu vistamenu =  new VistaMenu(this);
 
 
 public ControladorMenu () {
		this.vistamenu=new VistaMenu(this);
		this.vistamenu.setVisible(true);
		

	}




@Override
public void actionPerformed(ActionEvent e) {
	if(e.getSource().equals(getVistamenu().getBtnEmpezar())) {
		//new ControladorJuego();
		new VistaJuego().start();
	}
	if(e.getSource().equals(getVistamenu().getBtnOpciones())) {
		
	}
	if(e.getSource().equals(getVistamenu().getBtnSalir())) {
		vistamenu.dispose();
	}
}




public VistaMenu getVistamenu() {
	return vistamenu;
}




public void setVistamenu(VistaMenu vistamenu) {
	this.vistamenu = vistamenu;
}

}
