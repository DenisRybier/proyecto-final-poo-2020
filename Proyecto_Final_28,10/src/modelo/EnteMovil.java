package modelo;

import java.awt.image.BufferedImage;

public abstract class EnteMovil extends Ente {
	protected Posicion velocidad;
	protected double angulo;

	public EnteMovil(Posicion posicion, Posicion velocidad, BufferedImage imagen) {
		super(posicion, imagen);
		this.velocidad = velocidad;
	}

}
