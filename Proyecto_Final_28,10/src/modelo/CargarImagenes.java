package modelo;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CargarImagenes {

	public static BufferedImage Cargar(String path) {
		try {
			return ImageIO.read(CargarImagenes.class.getResource(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
