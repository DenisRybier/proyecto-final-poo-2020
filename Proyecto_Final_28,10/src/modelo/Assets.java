package modelo;

import java.awt.image.*;
import java.io.IOException;

public class Assets {
	
	public static BufferedImage personaje,personaje2,personajeUp,personaje3,personajeUp2,personajeUp3,personajeRight,personajeRight2,personajeRight3,personajeLeft,personajeLeft2,personajeLeft3, mapa;
	
	public static void init() {
		mapa = CargarImagenes.Cargar("/Escenario/mapa.png");
		
		//"personaje" es personaje mirando hacia abajo o mirando de frente 
		
		personaje = CargarImagenes.Cargar("/Personaje/front/quieto/quieto16.png");
		personaje2 = CargarImagenes.Cargar("/Personaje/front/camina/paso1.png");
		personaje3 = CargarImagenes.Cargar("/Personaje/front/camina/paso2.png");
		
		personajeUp = CargarImagenes.Cargar("/Personaje/back/quieto.png");
		personajeUp2 = CargarImagenes.Cargar("/Personaje/back/paso 1.png");
		personajeUp3 = CargarImagenes.Cargar("/Personaje/back/paso 2.png");
		
		personajeRight = CargarImagenes.Cargar("/Personaje/lateral/camina/imagenes/standing.png");
		personajeRight2 = CargarImagenes.Cargar("/Personaje/lateral/camina/imagenes/step1 right.png");
		personajeRight3 = CargarImagenes.Cargar("/Personaje/lateral/camina/imagenes/step2 right.png");
		
		personajeLeft = CargarImagenes.Cargar("/Personaje/lateral/camina/imagenes/standing left.png");
		personajeLeft2 = CargarImagenes.Cargar("/Personaje/lateral/camina/imagenes/step1 left.png");
		personajeLeft3 = CargarImagenes.Cargar("/Personaje/lateral/camina/imagenes/step2 left.png");
		
	}

}
