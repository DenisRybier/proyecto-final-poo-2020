package modelo;

import java.awt.*;
import java.awt.image.*;

public abstract class Ente {

	protected BufferedImage imagen;
	protected Posicion posicion;
	

	
	public Ente(Posicion posicion, BufferedImage imagen) {
		this.posicion = posicion;
		this.imagen = imagen;
	}

	public abstract void update();
	
	public abstract void draw(Graphics g);
	
	public abstract boolean collision();

	public Posicion getPosicion() {
		return posicion;
	}

	public void setPosicion(Posicion posicion) {
		this.posicion = posicion;
	}

	public BufferedImage getImagen() {
		return imagen;
	}

	public void setImagen(BufferedImage imagen) {
		this.imagen = imagen;
	}
	
	

	
}
