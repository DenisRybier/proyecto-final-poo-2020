package modelo;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import controlador.Teclado;

public class Personaje extends Ente{

	int animacion = 0;
	
	//velocidad del persona dependiendo la direccion que toma
	final int VEL = 3;
	
	public Personaje(Posicion posicion, BufferedImage imagen) {
		super(posicion, imagen);
	}
	

	@Override
	public void update() {
		//ES PARA ACTUALIZAR LA POSICION (X,Y),  Y QUE SE MUEVA EL OBJETO EN CUESTION.
		
		//SE ACTUALIZARA EN BASE A LO QUE INGRESEMOS POR EL TECLADO (W, A, S, D).
		
		/* POR EJEMPLO:
		 */

		
		
		if(animacion < 32767) {
			animacion++;
		}else {
			animacion = 0;
		}
		
		if(Teclado.UP) {			
				setImagen(Assets.personajeUp);			
				if(animacion % 10 > 5) {
					setImagen(Assets.personajeUp2);
				}else {
					setImagen(Assets.personajeUp3);				
			}
			
			posicion.setY(posicion.getY() - VEL); 									
		}			
		  
		
		if(Teclado.LEFT) {
			setImagen(Assets.personajeLeft);
			
			if(animacion % 10 > 5) {
				
				setImagen(Assets.personajeLeft2);
				
			}else {
				

				setImagen(Assets.personajeLeft3);
				}				
			
		  	posicion.setX(posicion.getX() - VEL);
		}

		if(Teclado.DOWN) {
			
			setImagen(Assets.personaje);
			
			if(animacion % 10 > 5) {
				//setImagen(Assets.personaje);
				setImagen(Assets.personaje2);
			}else {
				setImagen(Assets.personaje3);
			
		}
			posicion.setY(posicion.getY() + VEL);
		}
		
		if(Teclado.RIGHT) {
			
			setImagen(Assets.personajeRight);
			
			if(animacion % 10 > 5) {
				setImagen(Assets.personajeRight2);
			}else {
				setImagen(Assets.personajeRight3);
			
		}
		  	posicion.setX(posicion.getX() + VEL);
		}
		
		  
		 /* 
		 *TAMBIEN UNA VELOCIDAD OBTENIDA DE LA CLASE POSICION.
		*/ 
		
		
		//posicion = posicion.agregar(velocidad);
	}



	@Override
	public void draw(Graphics g) {
		
		g.drawImage(imagen, (int)posicion.getX(), (int)posicion.getY(), null); 
		
		//CONVERTIMOS A ENTERO PORQUE NO EXISTEN
	//VALORES FLOTANTES DE PIXELES.
	// EN UNA CLASE DETERMINADA QUE HAGA REFERENCIA A LA CLASE DEL ESTADO 
		//ACTUAL DEL JUEGO O LA EJECUCION DEL MISMO,
		// SE LLAMARA A UN OBJETO DE ESTA CLASE, PUES EN LA HERENCIA TRABAJAMOS CON LAS 
		//CLASES HIJAS, NO CON EL PADRE.
		
		//HABR�N OJBETOS PARA OBTENER LA POSICION Y PARA CAMBIAR LA IMAGEN SEGUN LA DIRECCION 
		//EN LA QUE ESTE YENDO.
		
	}

	
	@Override
	public boolean collision() {
		
		return false;
	}

}
