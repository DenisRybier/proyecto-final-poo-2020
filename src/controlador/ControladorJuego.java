package controlador;

import java.awt.*;

import modelo.Assets;
import modelo.Personaje;
import modelo.Posicion;
import vista.VistaJuego;

public class ControladorJuego {
	
	private Personaje personaje;
	private VistaJuego juego;
	

	public ControladorJuego() {
		personaje = new Personaje(new Posicion(200, 200), Assets.personaje);
	}

	public void update() {
		personaje.update();
	}

	public void draw(Graphics g) {
		personaje.draw(g);
	}

}
