package vista;

import javax.swing.*;
import java.awt.*;



import controlador.ControladorMenu;


public class VistaMenu extends JFrame {

public static final int WIDTH = 640, HEIGHT = 480;
private ControladorMenu controlador;
private JButton btnEmpezar;
private JButton btnOpciones;
private JButton btnSalir;
private JLabel lblFondoMenu;
	
	

	
	public VistaMenu(ControladorMenu controlador) {
		this.setControlador(controlador);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Menu Principal");
		setSize(WIDTH,HEIGHT);
		setResizable(false);
		//setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setVisible(true);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
		
		
		lblFondoMenu = new JLabel();
		lblFondoMenu.setIcon(new ImageIcon(getClass().getResource("/fondo/fondo-textura-grunge_1048-11687.jpg")));
		lblFondoMenu.setBounds(0,-20,WIDTH,HEIGHT);
		add(lblFondoMenu);

		
		btnEmpezar = new JButton("EMPEZAR");
		btnEmpezar.setFont(new Font("Yu Gothic Light", Font.BOLD | Font.ITALIC, 11));
		btnEmpezar.setBackground(Color.BLACK);
		btnEmpezar.setForeground(Color.RED);
		btnEmpezar.setBounds(270, 50, 120, 40);
		getContentPane().add(btnEmpezar);
		btnEmpezar.addActionListener(getControlador());
		
		btnOpciones = new JButton("OPCIONES");
		btnOpciones.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnOpciones.setForeground(Color.RED);
		btnOpciones.setBackground(Color.BLACK);
		btnOpciones.setBounds(270, 150, 120, 40);
		getContentPane().add(btnOpciones);
		btnOpciones.addActionListener(getControlador());
		
		btnSalir = new JButton("SALIR");
		btnSalir.setFont(new Font("Yu Gothic UI Light", Font.BOLD | Font.ITALIC, 11));
		btnSalir.setForeground(Color.RED);
		btnSalir.setBackground(Color.BLACK);
		btnSalir.setBounds(270, 250, 120, 40);
		getContentPane().add(btnSalir);
		
		JLabel label = new JLabel("");
		label.setBounds(0, 0, 794, 572);
		getContentPane().add(label);
		btnSalir.addActionListener(getControlador());
		
		
	}





	public ControladorMenu getControlador() {
		return controlador;
	}





	public void setControlador(ControladorMenu controlador) {
		this.controlador = controlador;
	}





	public JButton getBtnEmpezar() {
		return btnEmpezar;
	}





	public JButton getBtnOpciones() {
		return btnOpciones;
	}





	public JButton getBtnSalir() {
		return btnSalir;
	}
}
