package vista;

import java.awt.*;
import java.awt.image.*;
import java.io.IOException;

import javax.swing.*;

import controlador.ControladorJuego;
import controlador.Teclado;
import modelo.Assets;


public class VistaJuego extends JFrame implements Runnable {

	public static final int WIDTH = 1200, HEIGHT = 600;
	private Canvas canvas;
	private Thread thread;
	private Boolean running = false;
	
	private BufferStrategy bufferstrategy;
	private Graphics g;
	
	private final int FPS = 60;
	private double targetTime = 1000000000/FPS;
	private double deltaSec = 0;
	private int averageTime = FPS;
	
	private ControladorJuego controlador;
	private Teclado teclado;
	
	
	 public VistaJuego() {

		 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 setSize(WIDTH, HEIGHT);
		 setLocationRelativeTo(null);
		 setTitle("Unnamed Game");
		 setResizable(false);
		 setVisible(true);
		 
		 canvas = new Canvas();
		 teclado = new Teclado();
		 
		 canvas.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		 canvas.setMaximumSize(new Dimension(WIDTH, HEIGHT));
		 canvas.setMinimumSize(new Dimension(WIDTH, HEIGHT));
		 canvas.setFocusable(true);
		 
		 add(canvas);
		 canvas.addKeyListener(teclado);
	 }
	 
	 private void update() {
		 teclado.update();
		 controlador.update();
	 }
	 
	 private void draw() {
		 
		 bufferstrategy = canvas.getBufferStrategy();
		 
		 if(bufferstrategy == null) {
			 canvas.createBufferStrategy(3);
			 return;
		 }
		 
		 g = bufferstrategy.getDrawGraphics();
		 ////////////////////////////////////
		 
		 
		 g.setColor(Color.BLACK);
		 
		 g.fillRect(0, 0, WIDTH, HEIGHT);
		 
		 controlador.draw(g);
		 		 
		 
		 ////////////////////////////////////
		 g.dispose();
		 bufferstrategy.show();
	 }
	 
	 
	 private void init() {
		 Assets.init();
		 controlador = new ControladorJuego();
	 }


	@Override
	public void run() {
		
		long now = 0;
		long lastTime = System.nanoTime();
		
		init();		
		
		while(running) {
			now = System.nanoTime();
			deltaSec += (now - lastTime)/targetTime;
			lastTime = now;
			
			if(deltaSec >= 1) {
				update();
				draw();
				deltaSec--;
			}
			
		}
		stop();
	}

	 
	 public void start() {
		 thread = new Thread(this);
		 thread.start();
		 running = true;
	 }
	 
	 private void stop() {
		 try {
			thread.join();
			running = false;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	 }
	 
	 
	 
}
