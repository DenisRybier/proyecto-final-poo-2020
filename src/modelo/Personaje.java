package modelo;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import controlador.Teclado;

public class Personaje extends Ente {

	public Personaje(Posicion posicion, BufferedImage imagen) {
		super(posicion, imagen);
	}
	

	@Override
	public void update() {
		//ES PARA ACTUALIZAR LA POSICION (X,Y),  Y QUE SE MUEVA EL OBJETO EN CUESTION.
		
		//SE ACTUALIZARA EN BASE A LO QUE INGRESEMOS POR EL TECLADO (W, A, S, D).
		
		/* POR EJEMPLO:
		 */
		
		if(Teclado.UP)
			posicion.setY(posicion.getY() - 1);	
		  
		if(Teclado.LEFT)
		  	posicion.setX(posicion.getX() - 1);

		if(Teclado.DOWN)
			posicion.setY(posicion.getY() + 1);
		
		if(Teclado.RIGHT)
		  	posicion.setX(posicion.getX() + 1);
		  
		 /* 
		 *TAMBIEN UNA VELOCIDAD OBTENIDA DE LA CLASE POSICION.
		*/ 
		
		
		//posicion = posicion.agregar(velocidad);
	}



	@Override
	public void draw(Graphics g) {
		
		g.drawImage(imagen, (int)posicion.getX(), (int)posicion.getY(), null); 
		
		//CONVERTIMOS A ENTERO PORQUE NO EXISTEN
	//VALORES FLOTANTES DE PIXELES.
	// EN UNA CLASE DETERMINADA QUE HAGA REFERENCIA A LA CLASE DEL ESTADO 
		//ACTUAL DEL JUEGO O LA EJECUCION DEL MISMO,
		// SE LLAMARA A UN OBJETO DE ESTA CLASE, PUES EN LA HERENCIA TRABAJAMOS CON LAS 
		//CLASES HIJAS, NO CON EL PADRE.
		
		//HABR�N OJBETOS PARA OBTENER LA POSICION Y PARA CAMBIAR LA IMAGEN SEGUN LA DIRECCION 
		//EN LA QUE ESTE YENDO.
		
	}

	
	@Override
	public boolean collision() {
		
		return false;
	}
}
