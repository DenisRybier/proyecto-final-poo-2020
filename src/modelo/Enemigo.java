package modelo;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Enemigo extends Ente {

	public Enemigo(Posicion posicion, BufferedImage imagen) {
		super(posicion, imagen);
	}

	@Override
	public void update() {
		/*
		 * TENDRA POSICION, DIRECCION, y VELOCIDAD.
		 * NO DEPENDERA DEL TECLADO, YA QUE LOS ENEMIGOS TENDRAN UN PATRON DE MOVIMIENTO
		 * TOTALMENTE DIFERENTE AL PERSONAJE PRINCIPAL.
		 * 
		 * 
		 */
	}

	@Override
	public void draw(Graphics g) {
		/*TAMBIEN TENDRAN POSICION, UNA IMAGEN DIFERENTE DEPENDIENDO DE SU DIRECCION
		 * 
		 *  EJEMPLO:
		 *
		 * Graphics2D g2d = (Graphics2D)g;
		 * 
		 * 
		 * 
		 * 
		 */
	}

	@Override
	public boolean collision() {
		// TODO Auto-generated method stub
		return false;
	}
	
	

}
