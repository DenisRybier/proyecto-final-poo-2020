package modelo;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JPanel;




public class Dibujar extends JPanel {

	private JPanel contentPane;
    
    ImageIcon image_icon;
	public Dibujar() {
		 setFocusable(true);
		 setBackground(Color.BLACK);
		 setDoubleBuffered(true);
         image_icon = new ImageIcon("Escenario/mapa.png");

		     
	}
	
	public void paint(Graphics g) {
		 super.paint(g);

		 Graphics2D g2d = (Graphics2D)g;
		 g2d.drawImage(image_icon.getImage(), 0, 0, this);       
		 Toolkit.getDefaultToolkit().sync();
		 g.dispose();
		 }}
	

